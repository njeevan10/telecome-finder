webpackJsonp([0],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OperatorService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OperatorService = /** @class */ (function () {
    function OperatorService() {
        if (!this.operators) {
            this.operators = [{
                    _id: "1",
                    name: "A",
                    prefix_list: [{
                            _id: "1.1",
                            prefix_no: "224",
                            price: 1.4
                        }, {
                            _id: "1.2",
                            prefix_no: "22",
                            price: 1
                        }]
                }, {
                    _id: "2",
                    name: "B",
                    prefix_list: [{
                            _id: "2.1",
                            prefix_no: "224",
                            price: 4
                        }]
                }];
        }
    }
    OperatorService.prototype.getOperators = function () {
        return this.operators;
    };
    OperatorService.prototype.setOperators = function (list) {
        this.operators = list;
    };
    OperatorService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], OperatorService);
    return OperatorService;
}());

//# sourceMappingURL=operator.service.js.map

/***/ }),

/***/ 109:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 109;

/***/ }),

/***/ 150:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 150;

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_operator_service__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, optService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.optService = optService;
        this.calculate = function () {
            _this.optimalOperator = {};
            var searchNo = _this.teleNumber;
            var searchLst = [];
            __WEBPACK_IMPORTED_MODULE_2_lodash__["each"](_this.operators, function (opList) {
                __WEBPACK_IMPORTED_MODULE_2_lodash__["each"](opList.prefix_list, function (lst, key) {
                    if (searchNo.startsWith(lst.prefix_no)) {
                        searchLst.push({
                            index: key,
                            len: lst.prefix_no.length,
                            operator_id: opList._id,
                            operator_name: opList.name,
                            prefix: lst.prefix_no,
                            cost: lst.price
                        });
                    }
                });
            });
            var resultObj = __WEBPACK_IMPORTED_MODULE_2_lodash__["maxBy"](searchLst, function (lst) {
                return lst.len;
            });
            var list = __WEBPACK_IMPORTED_MODULE_2_lodash__["filter"](searchLst, function (lst) {
                return resultObj.prefix == lst.prefix;
            });
            var optimalOperator = __WEBPACK_IMPORTED_MODULE_2_lodash__["minBy"](list, function (lst) {
                return lst.cost;
            });
            if (optimalOperator) {
                _this.optimalOperator = optimalOperator;
                _this.isOpFound = true;
            }
            else {
                _this.isOpFound = false;
            }
        };
        this.operators = optService.getOperators();
        this.optimalOperator = {};
        this.isOpFound = false;
        this.op = {
            _id: "",
            name: "",
            prefix_list: [{
                    _id: "",
                    prefix_no: "",
                    price: 0
                }]
        };
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/jeeva/Projects/ionic/telecom_finder/src/pages/home/home.html"*/'<!-- <ion-header>\n    <ion-navbar>\n        <ion-title>\n            Telephone Operator\n        </ion-title>\n    </ion-navbar>\n</ion-header> -->\n\n<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n        <ion-title>Telephone Operator Search</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <ion-item>\n        <ion-label floating>Enter Telephone Number </ion-label>\n        <ion-input limit-to="20" type="number" [(ngModel)]="teleNumber"></ion-input>\n    </ion-item>\n    <ion-buttons right>\n        <button ion-button padding (click)="getOptimalOpt()">\n      <ion-icon name="call"></ion-icon> &nbsp;Search Operator \n    </button>\n    </ion-buttons>\n    <ion-row>\n        <ion-col col>\n            <ion-label *ngIf="isOpFound">Operator: <span class="highlit">{{optimalOperator.operator_name}}</span> and Cost : <span class="highlit">{{optimalOperator.cost}}</span></ion-label>\n        </ion-col>\n    </ion-row>\n    <!-- <ion-row>\n        <ion-col col><span *ngIf="!optimalOperator"> No Operator found</span></ion-col>\n    </ion-row> -->\n</ion-content>'/*ion-inline-end:"/home/jeeva/Projects/ionic/telecom_finder/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__services_operator_service__["a" /* OperatorService */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_operator_service__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SettingsPage = /** @class */ (function () {
    function SettingsPage(navCtrl, optService, toastr) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.optService = optService;
        this.toastr = toastr;
        this.mode = 'opt_list';
        this.saveOperator = function () {
            var opId = undefined;
            var prefix_list = [];
            var existObj;
            var opIndex;
            if (_this.validateOpertaor() == 0) {
                //Check the Opertor already exist by name
                if (_this.operators && _this.operators.length > 0) {
                    opIndex = _this.operators.findIndex(function (i) { return i.name.toLowerCase() === _this.op.name.toLowerCase(); });
                }
                if (opIndex > -1) {
                    existObj = _this.operators[opIndex];
                    opId = _this.operators[opIndex]._id;
                    __WEBPACK_IMPORTED_MODULE_3_lodash__["each"](_this.op.prefix_list, function (pref, key) {
                        var preId = opId + "." + (key + 1);
                        var prefIndex = existObj.prefix_list.findIndex(function (opObj) { return opObj.prefix_no.toLowerCase() === pref.prefix_no.toLowerCase(); });
                        if (prefIndex > -1) {
                            existObj.prefix_list[prefIndex].prefix_no = pref.prefix_no;
                            existObj.prefix_list[prefIndex].price = pref.price;
                        }
                        else {
                            existObj.prefix_list.push({
                                _id: preId,
                                prefix_no: pref.prefix_no,
                                price: pref.price
                            });
                        }
                    });
                    //this.operators[opIndex].name = this.op.name;
                    //this.operators[opIndex].prefix_list = this.op.prefix_list
                }
                else {
                    opId = _this.operators.length + 1;
                    __WEBPACK_IMPORTED_MODULE_3_lodash__["each"](_this.op.prefix_list, function (pref, key) {
                        var preId = opId + "." + (key + 1);
                        var prefIndex = prefix_list.findIndex(function (opObj) { return opObj.prefix_no.toLowerCase() === pref.prefix_no.toLowerCase(); });
                        if (prefIndex == -1) {
                            prefix_list.push({
                                _id: preId,
                                prefix_no: pref.prefix_no,
                                price: pref.price
                            });
                        }
                    });
                    _this.operators.push({
                        _id: opId.toString(),
                        name: _this.op.name,
                        prefix_list: prefix_list
                    });
                }
                _this.optService.setOperators(_this.operators);
                _this.cancel();
                _this.showToast("Added successfully..");
            }
            else {
                _this.showToast("Please fill all required fields..");
            }
        };
        this.updateOperator = function () {
            var opIndex;
            var prefix_list = [];
            if (_this.validateOpertaor() == 0) {
                //Check the Operator already exist by name
                if (_this.operators && _this.operators.length > 0) {
                    opIndex = _this.operators.findIndex(function (i) { return i.name.toLowerCase() === _this.op.name.toLowerCase() && i._id != _this.op._id; });
                }
                if (opIndex > -1) {
                    _this.cancel();
                    _this.showToast("Sorry, There is an operator with the same name");
                }
                else {
                    __WEBPACK_IMPORTED_MODULE_3_lodash__["each"](_this.op.prefix_list, function (pref, key) {
                        var preId = _this.op._id + "." + (key + 1);
                        var prefIndex = prefix_list.findIndex(function (opObj) { return opObj.prefix_no.toLowerCase() === pref.prefix_no.toLowerCase(); });
                        if (prefIndex == -1) {
                            prefix_list.push({
                                _id: preId,
                                prefix_no: pref.prefix_no,
                                price: pref.price
                            });
                        }
                    });
                    var editObjIndex = _this.operators.findIndex(function (i) { return i._id === _this.op._id; });
                    if (editObjIndex > -1) {
                        _this.operators[editObjIndex].name = _this.op.name;
                        _this.operators[editObjIndex].prefix_list = _this.op.prefix_list;
                    }
                    _this.cancel();
                    _this.showToast("Updated successfully..");
                }
            } //End Validation
        };
        this.cancel = function () {
            _this.op = {
                _id: "",
                name: "",
                prefix_list: [{
                        _id: "",
                        prefix_no: "",
                        price: undefined
                    }]
            };
            _this.editId = undefined;
            _this.mode = 'opt_list';
        };
        this.showDetails = function (op) {
            __WEBPACK_IMPORTED_MODULE_3_lodash__["each"](_this.operators, function (eOp) {
                if (eOp._id === op._id) {
                    eOp.show = !eOp.show;
                }
                else {
                    eOp.show = false;
                }
            });
        };
        this.delete = function (op) {
            if (_this.editId != op._id) {
                var opIndex = _this.operators.findIndex(function (i) { return i._id === op._id; });
                if (opIndex > -1) {
                    _this.operators.splice(opIndex, 1);
                    _this.optService.setOperators(_this.operators);
                    _this.showToast("Removed successfully..");
                }
            }
            else {
                _this.showToast("Sorry, You couldn't delete while editing...");
            }
        };
        this.addPrefix = function () {
            if (_this.op && _this.op.prefix_list.length < 5) {
                _this.op.prefix_list.push({
                    _id: "",
                    prefix_no: "",
                    price: undefined
                });
            }
            else {
                _this.showToast("You can't add more than 5");
            }
        };
        this.removePrefix = function (index) {
            _this.op.prefix_list.splice(index, 1);
        };
        this.edit = function (op) {
            _this.op = op;
            _this.editId = op._id;
            _this.mode = 'add_op';
        };
        this.operators = optService.getOperators();
        this.op = {
            _id: "",
            name: "",
            prefix_list: [{
                    _id: "",
                    prefix_no: "",
                    price: undefined
                }]
        };
    }
    SettingsPage.prototype.validateOpertaor = function () {
        var errorCnt = 0;
        this.op.name_error = undefined;
        if (!this.op.name) {
            this.op.name_error = "Name is required";
            errorCnt++;
        }
        __WEBPACK_IMPORTED_MODULE_3_lodash__["each"](this.op.prefix_list, function (lst) {
            lst.prefix_error = undefined;
            lst.price_error = undefined;
            if (!lst.prefix_no) {
                lst.prefix_error = "Prefix is required";
                errorCnt++;
            }
            if (!lst.price) {
                lst.price_error = "Price is required";
                errorCnt++;
            }
        });
        return errorCnt;
    };
    SettingsPage.prototype.showToast = function (info) {
        this.infoTst = this.toastr.create({
            message: info,
            duration: 3000,
            showCloseButton: true,
            closeButtonText: "Ok",
            position: 'bottom'
        });
        this.infoTst.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        this.infoTst.present();
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"/home/jeeva/Projects/ionic/telecom_finder/src/pages/settings/settings.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n        <ion-title>Settings</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n    <ion-segment [(ngModel)]="mode">\n        <ion-segment-button value="opt_list">\n            Operator List\n        </ion-segment-button>\n        <ion-segment-button value="add_op">\n            Operator Console\n        </ion-segment-button>\n    </ion-segment>\n\n    <div [ngSwitch]="mode">\n\n        <div *ngSwitchCase="\'opt_list\'">\n\n            <ion-list>\n                <ion-item *ngFor="let op of operators">\n                    <ion-row>\n                        <div col ion-button icon-only clear (click)="showDetails(op)" class="left-text">\n                            <ion-icon name="information-circle"></ion-icon> &nbsp;{{op.name}}\n                        </div>\n                        <!-- <div col text-center>{{op.name}}</div> -->\n                        <div col-1 ion-button icon-only clear small (click)="edit(op)">\n                            <ion-icon name="create"></ion-icon>\n                        </div>\n                        <div col-1 ion-button icon-only clear small (click)="delete(op)">\n                            <ion-icon name="trash"></ion-icon>\n                        </div>\n                    </ion-row>\n                    <ion-item *ngIf="op.show && op.prefix_list.length<=0">\n                        <ion-label>No Prefixes added</ion-label>\n                    </ion-item>\n                    <ion-list *ngIf="op.show && op.prefix_list.length>0">\n                        <ion-item>\n                            <ion-row>\n                                <div col class="header">Prefix</div>\n                                <div col class="header">Cost</div>\n                            </ion-row>\n                        </ion-item>\n                        <ion-item *ngFor="let pre of op.prefix_list">\n                            <ion-row>\n                                <div col class="content">{{pre.prefix_no}}</div>\n                                <div col class="content">{{pre.price}}</div>\n                            </ion-row>\n                        </ion-item>\n                    </ion-list>\n                </ion-item>\n            </ion-list>\n\n\n        </div>\n\n        <div *ngSwitchCase="\'add_op\'">\n            <ion-item>\n                <ion-label floating [ngClass]="op.name_error ?\'error-msg\':\'normal-msg\'"> Enter Operator Name</ion-label>\n                <ion-input limit-to="12" type="text" [(ngModel)]="op.name"></ion-input>\n            </ion-item>\n\n            <ion-buttons right>\n                <button ion-button padding (click)="cancel()">\n          <ion-icon name="md-close"></ion-icon>&nbsp;Cancel\n        </button>\n                <button ion-button padding (click)="saveOperator()" *ngIf="!this.editId">\n          <ion-icon name="checkmark-circle"></ion-icon>&nbsp;Save Operator\n        </button>\n\n                <button ion-button padding (click)="updateOperator()" *ngIf="this.editId">\n          <ion-icon name="checkmark-circle"></ion-icon>&nbsp;Update Operator\n        </button>\n                <button ion-button icon-only clear (click)="addPrefix()" *ngIf="!this.editId">\n          <ion-icon name="add-circle"></ion-icon>\n        </button>\n            </ion-buttons>\n\n            <ion-row *ngFor="let pre of op.prefix_list; let first = first;">\n                <ion-item col>\n                    <ion-label floating [ngClass]="pre.prefix_error ?\'error-msg\':\'normal-msg\'"> Enter Prefix</ion-label>\n                    <ion-input limit-to="10" type="text" [(ngModel)]="pre.prefix_no"></ion-input>\n                </ion-item>\n                <ion-item col>\n                    <ion-label floating [ngClass]="pre.price_error ?\'error-msg\':\'normal-msg\'"> Enter Price</ion-label>\n                    <ion-input limit-to="4" type="number" [(ngModel)]="pre.price"></ion-input>\n                </ion-item>\n                <button ion-button icon-only clear (click)="removePrefix()" *ngIf="!first" class="danger">\n          <ion-icon name="md-close"></ion-icon>\n        </button>\n            </ion-row>\n\n        </div>\n\n    </div>\n\n\n\n\n\n\n\n    <!-- <ion-list>\n        <button ion-item *ngFor="let item of items" (click)="itemclickped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n    </ion-list>\n    <div *ngIf="selectedItem" padding>\n        You navigated here from <b>{{selectedItem.title}}</b>\n    </div> -->\n</ion-content>'/*ion-inline-end:"/home/jeeva/Projects/ionic/telecom_finder/src/pages/settings/settings.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_operator_service__["a" /* OperatorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_operator_service__["a" /* OperatorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ToastController */]) === "function" && _c || Object])
    ], SettingsPage);
    return SettingsPage;
    var _a, _b, _c;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(220);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_settings_settings__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_operator_service__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_settings_settings__["a" /* SettingsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_settings_settings__["a" /* SettingsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_8__services_operator_service__["a" /* OperatorService */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_settings_settings__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'Settings', component: __WEBPACK_IMPORTED_MODULE_5__pages_settings_settings__["a" /* SettingsPage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/jeeva/Projects/ionic/telecom_finder/src/app/app.html"*/'<ion-menu [content]="content">\n    <ion-header>\n        <ion-toolbar>\n            <ion-title>Menu</ion-title>\n        </ion-toolbar>\n    </ion-header>\n\n    <ion-content>\n        <ion-list>\n            <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n        </ion-list>\n    </ion-content>\n\n</ion-menu>\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/home/jeeva/Projects/ionic/telecom_finder/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[197]);
//# sourceMappingURL=main.js.map