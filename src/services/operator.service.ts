import { Injectable } from '@angular/core';
import { Operator } from '../imports/modals/operator';


@Injectable()
export class OperatorService {

    private operators: Array<Operator>;

     constructor() {
        if(!this.operators){
            this.operators = [{
                _id: "1",
                name: "A",
                prefix_list: [{
                    _id: "1.1",
                    prefix_no: "1",
                    price: 0.9
                }, {
                    _id: "1.2",
                    prefix_no: "268",
                    price: 5.1
                },{
                    _id: "1.3",
                    prefix_no: "46",
                    price: 0.17
                },{
                    _id: "1.4",
                    prefix_no: "4620",
                    price: 0.0
                },{
                    _id: "1.5",
                    prefix_no: "468",
                    price: 0.15
                },{
                    _id: "1.6",
                    prefix_no: "4631",
                    price: 0.15
                },{
                    _id: "1.7",
                    prefix_no: "4673",
                    price: 0.9
                },{
                    _id: "1.8",
                    prefix_no: "46732",
                    price: 1.1
                }]
            }, {
                _id: "2",
                name: "B",
                prefix_list: [{
                    _id: "2.1",
                    prefix_no: "1",
                    price: 0.92
                },{
                    _id: "2.2",
                    prefix_no: "44",
                    price: 0.5
                },{
                    _id: "2.3",
                    prefix_no: "46",
                    price: 0.2
                },{
                    _id: "2.4",
                    prefix_no: "467",
                    price: 1.0
                },{
                    _id: "2.5",
                    prefix_no: "48",
                    price: 1.2
                }]
            }]
        }
    } 



    getOperators():Array<Operator> {
        return this.operators
    }


    setOperators(list:Array<Operator>):void{
        this.operators = list
    }

}