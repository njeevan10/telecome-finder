import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { OperatorService } from '../../services/operator.service';
import { Operator, Prefix } from '../../imports/modals/operator';
import * as _ from "lodash";

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  //List of operators
  operators: Array<Operator>;

  //Object to save/edit the operator
  op: Operator;

  //Object to hold toastr
  infoTst: any;

  //Current Segment
  mode: string = 'opt_list'

  //Operator's edit operation Id
  editId: string;

  //Operator's name
  editOpName:string


  
  constructor(public navCtrl: NavController,
    private optService: OperatorService,
    private toastr: ToastController
  ) {
    //Get the latest oprator list
    this.operators = optService.getOperators();
  }

  /**
   * Life Cycle event
   */
  ionViewWillEnter() {
    this.op = {
      _id: "",
      name: "",
      prefix_list: [{
        _id: "",
        prefix_no: "",
        price: undefined
      }]
    }
  }

  /**
   * Life Cycle event
   */
  ionViewWillLeave() {
    this.operators = undefined;
    this.op = undefined;
    this.infoTst = undefined;
  }


  /**
   * To validate the operator before add/edit
   */
  validateOpertaor() {
    let errorCnt = 0

    this.op.name_error = undefined

    if (!this.op.name) {
      this.op.name_error = "Name is required"
      errorCnt++;
    }

    _.each(this.op.prefix_list, (lst) => {

      lst.prefix_error = undefined
      lst.price_error = undefined

      if (!lst.prefix_no) {
        lst.prefix_error = "Prefix is required"
        errorCnt++;
      }

      if (!lst.price) {
        lst.price_error = "Price is required"
        errorCnt++;
      }
    })

    return errorCnt
  }


  /**
   * To add new operator
   */
  saveOperator = () => {
    let opId = undefined
    let prefix_list = [];
    let existObj: Operator;
    let opIndex: number;

    if (this.validateOpertaor() == 0) {

      //Check the Opertor already exist by name
      if (this.operators && this.operators.length > 0) {
        opIndex = this.operators.findIndex(i => i.name.toLowerCase() === this.op.name.toLowerCase())
      }

      //If Exist push added prefix data to exixt opertaor
      //Otherwise add it as new operator
      if (opIndex > -1) {
        existObj = this.operators[opIndex]
        opId = this.operators[opIndex]._id

        _.each(this.op.prefix_list, (pref: Prefix, key) => {
          let preId = opId + "." + (key + 1)
          let prefIndex = existObj.prefix_list.findIndex(opObj => opObj.prefix_no.toLowerCase() === pref.prefix_no.toLowerCase())

          if (prefIndex > -1) {
            existObj.prefix_list[prefIndex].prefix_no = pref.prefix_no
            existObj.prefix_list[prefIndex].price = pref.price
          } else {
            existObj.prefix_list.push({
              _id: preId,
              prefix_no: pref.prefix_no,
              price: pref.price
            })
          }
        })
      } else {
        opId = this.operators.length + 1;
        _.each(this.op.prefix_list, (pref, key) => {
          let preId = opId + "." + (key + 1)

          //To avoid duplication
          let prefIndex = prefix_list.findIndex(opObj => opObj.prefix_no.toLowerCase() === pref.prefix_no.toLowerCase())

          if (prefIndex == -1) {
            prefix_list.push({
              _id: preId,
              prefix_no: pref.prefix_no,
              price: pref.price
            })
          }
        })

        this.operators.push({
          _id: opId.toString(),
          name: this.op.name,
          prefix_list: prefix_list
        })

      }

      this.optService.setOperators(this.operators)

      this.cancel()
      this.showToast("Added successfully..")

    } else {
      this.showToast("Please fill all required fields..")
    }
  }

  /**
   * To update the edited opertaor
   */
  updateOperator = () => {
    let opIndex: number;
    let prefix_list = [];

    //Validation Starts
    if (this.validateOpertaor() == 0) {
      //Check the Operator already exist by name
      if (this.operators && this.operators.length > 0) {
        opIndex = this.operators.findIndex(i => i.name.toLowerCase() === this.op.name.toLowerCase() && i._id != this.op._id)
      }

      //If the name is already exist, just cancel the operation; ToDo : Push edited list to existed list, if needed
      //Otherwise update the operator
      if (opIndex > -1) {
        this.showToast("Sorry, There is an operator with the same name")
        this.op.name = this.editOpName
      } else {
        _.each(this.op.prefix_list, (pref, key) => {
          let preId = this.op._id + "." + (key + 1)
          let prefIndex = prefix_list.findIndex(opObj => opObj.prefix_no.toLowerCase() === pref.prefix_no.toLowerCase())

          if (prefIndex == -1) {
            prefix_list.push({
              _id: preId,
              prefix_no: pref.prefix_no,
              price: pref.price
            })
          }
        })

        let editObjIndex = this.operators.findIndex(i => i._id === this.op._id)
        if (editObjIndex > -1) {
          this.operators[editObjIndex].name = this.op.name;
          this.operators[editObjIndex].prefix_list = this.op.prefix_list
        }
        this.showToast("Updated successfully..")
      }
      //Get the latest Operator
      this.operators = this.optService.getOperators();
      this.cancel()
    }//End Validation
  }

  /**
   * To cancel the current operation(save/edit)
   */
  cancel = () => {
    this.op = {
      _id: "",
      name: undefined,
      prefix_list: [{
        _id: "",
        prefix_no: "",
        price: undefined
      }]
    }
    this.editId = undefined
    this.mode = 'opt_list'
  }

  /**
   * To expand the selected operator list
   */
  showDetails = (op: Operator) => {
    _.each(this.operators, (eOp: any) => {
      if (eOp._id === op._id) {
        eOp.show = !eOp.show
      } else {
        eOp.show = false
      }
    })
  }

  /**
   * To Delete the selected operator
   */
  delete = (op: Operator) => {
    if (this.editId != op._id) {
      let opIndex = this.operators.findIndex(i => i._id === op._id)
      if (opIndex > -1) {
        this.operators.splice(opIndex, 1);
        this.optService.setOperators(this.operators)
        this.showToast("Removed successfully..")
      }
    } else {
      this.showToast("Sorry, You couldn't delete while editing...")
    }
  }

  /**
   * To create new prefix GUI
   */
  addPrefix = () => {
    if (this.op && this.op.prefix_list.length < 5) {
      this.op.prefix_list.push({
        _id: "",
        prefix_no: "",
        price: undefined
      })
    } else {
      this.showToast("You can't add more than 5")
    }
  }

  /**
   * To remove the prefix GUI
   */
  removePrefix = (index) => {
    this.op.prefix_list.splice(index, 1);
  }


  /**
   * To edit the selected operator
   */
  edit = (op: Operator) => {
    this.op = op
    this.editOpName = op.name;
    this.editId = op._id
    this.mode = 'add_op'
  }

  
  /**
   * To create toastr and show toast message
   */
  showToast(info: string) {
    this.infoTst = this.toastr.create({
      message: info,
      duration: 3000,
      showCloseButton: true,
      closeButtonText: "Ok",
      position: 'bottom'
    })
    this.infoTst.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    this.infoTst.present()
  }
}
