import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Operator, Prefix } from '../../imports/modals/operator';

import * as _ from "lodash";
import { OperatorService } from '../../services/operator.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //Number to search the optimal operator
  teleNumber: string;

  //Object to hold optimal operator
  optimalOperator: any;

  //Boolean to set availability of operator
  isOpFound: Boolean;

  //List of operators
  operators: Array<Operator>;

  //Object to hold toastr
  infoTst: any;

  /**
   * Constructor
   */
  constructor(public navCtrl: NavController,
    private optService: OperatorService,
    private toastr: ToastController
  ) {

    //Get the latest oprator list
    this.operators = optService.getOperators();
    
  }


   /**
   * Life Cycle event
   */
  ionViewWillEnter() {
    this.optimalOperator = {};
  }

  /**
   * Life Cycle event
   */
  ionViewWillLeave() {
    this.operators = undefined
    this.optimalOperator = undefined
    this.infoTst = undefined
  }


  
  /**
   * To search optimal operator based on the search query
   */
  getOptimalOpt = () => {
    if (!this.teleNumber) {
      this.showToast("Please enter number to search..")
    } else {
      this.optimalOperator = {};
      let searchNo = this.teleNumber

      var searchLst = []

      //To listout all the prefixes that matches the given query
      _.each(this.operators, (opList: Operator) => {
        _.each(opList.prefix_list, (lst: Prefix, key) => {
          if (searchNo.startsWith(lst.prefix_no)) {
            searchLst.push({
              index: key,
              len: lst.prefix_no.length,
              operator_id: opList._id,
              operator_name: opList.name,
              prefix: lst.prefix_no,
              cost: lst.price
            })
          }
        })
      })

      //Get the max length which matches the given query
      let resultObj = _.maxBy(searchLst, (lst) => {
        return lst.len
      })

      //List out the max length prefix
      let list = _.filter(searchLst, (lst) => {
        return resultObj.prefix == lst.prefix
      })

      //Get the minimum cost provided operator
      let optimalOperator = _.minBy(list, (lst) => {
        return lst.cost
      })

      if (optimalOperator) {
        this.optimalOperator = optimalOperator;
        this.isOpFound = true;
      } else {
        this.isOpFound = false;
        this.showToast("Oops, Couldn't find any match...")
      }
      this.teleNumber = undefined
    }

  }

  /**
   * To create toastr and show toast message
   */
  showToast(info: string) {
    this.infoTst = this.toastr.create({
      message: info,
      duration: 3000,
      showCloseButton: true,
      closeButtonText: "Ok",
      position: 'bottom'
    })
    this.infoTst.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    this.infoTst.present()
  }

}
