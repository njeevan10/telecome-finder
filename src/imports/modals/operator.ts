export interface Operator {
    _id?: string;
    name?: string;
    name_error?:string;
    prefix_list?: Array<Prefix>;
}


export interface Prefix {
    _id?: string;
    prefix_no?: string;
    price?: number;
    prefix_error?:string;
    price_error?:string;
}